#!/usr/bin/env bash
# bin/compile <build-dir> <cache-dir>

# fail fast
set -e

# config
APACHE_VERSION="2.2.22"
APACHE_PATH="apache"
PHP_VERSION="5.3.10"
PHP_PATH="php"

BIN_DIR=$(dirname $0)
BUILD_DIR=$1
CACHE_DIR=$2
BUNDLE_DIR="${CACHE_DIR}/bundles"
LP_DIR=`cd $(dirname $0); cd ..; pwd`
export COMPOSER_HOME="${CACHE_DIR}/.composer"
COMPOSER_URL="http://getcomposer.org/composer.phar"

function indent() {
  c='s/^/       /'
  case $(uname) in
    Darwin) sed -l "$c";;
    *)      sed -u "$c";;
  esac
}

function download_url() {
  TARGET_URL="$1"
  curl -s -S -O -L -m 300 --connect-timeout 60 "$TARGET_URL"
}

# include .files when moving things around
shopt -s dotglob

mkdir -p $BUILD_DIR $CACHE_DIR ${BUNDLE_DIR} ${BUILD_DIR}/local ${BUILD_DIR}/vendor/bin ${COMPOSER_HOME}

cd $BUILD_DIR

# move app things to www
mkdir -p $CACHE_DIR/www
mv * $CACHE_DIR/www
mv $CACHE_DIR/www .

# keep Procfile
if [ -f www/Procfile ]; then
  mv www/Procfile .
fi

APACHE_URL="https://s3.amazonaws.com/php-lp/apache-$APACHE_VERSION.tar.gz"
echo "-----> Bundling Apache version $APACHE_VERSION"
curl --silent --max-time 60 --location "$APACHE_URL" | tar xz

PHP_URL="https://s3.amazonaws.com/php-lp/php-$PHP_VERSION.tar.gz"
echo "-----> Bundling PHP version $PHP_VERSION"
curl --silent --max-time 60 --location "$PHP_URL" | tar xz

# update config files
cp $LP_DIR/conf/httpd.conf $APACHE_PATH/conf
cp $LP_DIR/conf/php.ini php

# make php available on bin
mkdir -p bin
ln -s /app/php/bin/php bin/php

# Composer Installation
pushd ${BUILD_DIR} > /dev/null
if [ -f "composer.json" ]
then
  echo "-----> Installing dependencies using Composer"
  GIT_DIR_ORIG=${GIT_DIR}
  unset GIT_DIR

  if [ ! -f "composer.phar" ]
  then
    echo "Fetching composer.phar" | indent
    echo ${COMPOSER_URL} | indent
    download_url ${COMPOSER_URL}
  fi

  # do the deed!
  echo "Running: php composer.phar install" | indent
  LD_LIBRARY_PATH="${BUILD_DIR}/local/lib" ${BUILD_DIR}/vendor/php/bin/php composer.phar install -n | indent

  export GIT_DIR=${GIT_DIR_ORIG}
fi
popd > /dev/null

# check if we have Propel Migrations to run
if [ -f www/vendor/bin/propel-gen ]; then
  export LD_LIBRARY_PATH=$BUILD_DIR/php/ext
  export PATH=$BUILD_DIR/php/bin:$PATH
  echo "-----> Running Propel migrations"
  cd www
  vendor/bin/propel-gen ./ -Dpropel.buildtime.conf.file=herokutime-conf.xml migrate
  cd $BUILD_DIR
fi

cat >>boot.sh <<EOF
for var in \`env|cut -f1 -d=\`; do
  echo "PassEnv \$var" >> /app/apache/conf/httpd.conf;
done
touch /app/apache/logs/error_log
touch /app/apache/logs/access_log
tail -F /app/apache/logs/error_log &
tail -F /app/apache/logs/access_log &
export LD_LIBRARY_PATH=/app/php/ext
export PHP_INI_SCAN_DIR=/app/www
echo "Launching apache"
exec /app/apache/bin/httpd -DNO_DETACH
EOF

chmod +x boot.sh

# clean the cache
rm -rf $CACHE_DIR
